public class Account {

    private double balance;
    private String name;

    public static double interestRate = 45.69;

    public Account (String name, double balance){
        this.name = name;

        this.balance = balance;
    }

    public Account (){
        this("James", 50000);
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addInterest () {

      balance = balance * interestRate;

    }
    public static void setInterestRate(double interestRate)
	{
		interestRate = interestRate;
	}

	public static double getInterestRate()
	{
		return interestRate;
	}


}

