public class MyFirstClass {

    public static void main(String[] args) {
        String make = "Renault";
        String model = "Laguna";
        double engineSize = 1.8;
        byte gear = 6;
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        if(engineSize > 1.5){
            System.out.println("The "+ make + " " + model+ " is a powerful car.");
        } else {
            System.out.println("The "+ make + " " + model+ " is not a powerful car.");
        }

        // if (gear == 1){
        //     System.out.println("Car Should go 10 mph");
        // } else if ( gear == 2) {
        //     System.out.println("Car should go 20 mph");

        // } else if (gear >= 3){
        //     System.out.println("Car can go any speed");
        // }

        switch(gear) {
            case 1: 
            System.out.println("Car can go 10 mph");
            break;
            
            case 2: 
            System.out.println("Car can go 20 mph");
            break;

            case 3: 
            System.out.println("Car can go 30 mph");
            break;

            case 4: 
            System.out.println("Car can go 40 mph");
            break;

            case 5: 
            System.out.println("Car can go 50 mph");
            break;

            case 6: 
            System.out.println("Car can go 60 mph");
            break;

            case 7: 
            System.out.println("Car can go 70 mph");
            break;

            case 8: 
            System.out.println("Car can go 80 mph");
            break;

            case 9: 
            System.out.println("Car can go 90 mph");
            break;

            default:

            System.out.println("Car can probably go somewhere");
        }

        for(int year = 1900; year < 2001; year++){

            if (year % 4 == 0 ){
                System.out.println(year);
                if(year == 1920){
                    break;
                }
            }

        }



        

  }
}



    

